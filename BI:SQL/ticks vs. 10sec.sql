set dateformat ymd
go
drop table #au_10sec_2014
select *
into #au_10sec_2014
from stg.fund
where name_tool = 'AU'
  and Time_UTC between '2014-01-01 23:00:00.000' and '2014-01-04'
go
with set_of_dates as 
	(
		select 
			1 as num, 
			convert(datetime, '2014-01-01 23:00:00.000') as Time_UTC1
		union all
		select 
			num + 1, 
			dateadd(second, 10, Time_UTC1)
		from set_of_dates
		where dateadd(second, 10, Time_UTC1) <= '2014-01-03 21:59:50.000'
	) 
,ticks_with_time_sec as
	(
		select
			Time_UTC
			,Bid
			,BidVolume
			,Time_sec = case 
			when datepart(second, time_utc) >= 0 and datepart(second, time_utc) < 10 
			then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 0, 0)
			when datepart(second, time_utc) >= 10 and datepart(second, time_utc) < 20 
			then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 10, 0)
			when datepart(second, time_utc) >= 20 and datepart(second, time_utc) < 30 
			then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 20, 0)
			when datepart(second, time_utc) >= 30 and datepart(second, time_utc) < 40 
			then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 30, 0)
			when datepart(second, time_utc) >= 40 and datepart(second, time_utc) < 50 
			then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 40, 0)
			when datepart(second, time_utc) >= 50 and datepart(second, time_utc) <= 59 
			then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 50, 0)
			end
		from [test].[AUTicks_2014]
	)
,firstV_and_lastV_for_ticks as 
	(
		select 
			Time_UTC
			,Bid
			,BidVolume
			,Time_sec
			,firstV = first_value(Bid) over(partition by Time_sec order by time_utc)
			,lastV = last_value(Bid) over(partition by Time_sec 
								 	      order by Time_UTC  
								 	      rows between current row and unbounded following)
		from ticks_with_time_sec
	)
,grouped_ticks_10sec as 
	(
		select 
			Time_UTC = Time_sec
			,Open_t = max(firstV)
			,Close_t = max(lastV)
			,Volume_t = sum(BidVolume)
			,High_t = max(Bid)
			,Low_t = min(Bid)
		from firstV_and_lastV_for_ticks
		group by Time_sec
	)

select 
	a.Time_UTC
	,a.Open_t
	,a.Close_t
	,a.Volume_t
	,a.High_t
	,a.Low_t
	,b.Time_UTC1
into #grouped_ticks_10sec_with_null
from grouped_ticks_10sec a
full join set_of_dates b on a.Time_UTC = b.[Time_UTC1]
order by b.Time_UTC1
option(maxrecursion 0)

select 
	Time_UTC = isnull(Time_UTC, Time_UTC1)
	,Open_t = isnull(Open_t, b.Close_t)
	,Close_t = isnull(a.Close_t, b.Close_t)
	,Volume_t = isnull(Volume_t, 0)
	,High_t = isnull(High_t, b.Close_t)
	,Low_t = isnull(Low_t, b.Close_t)
into #ticks_10sec
from #grouped_ticks_10sec_with_null a outer apply
(select top (1) Close_t
from #grouped_ticks_10sec_with_null
where a.Close_t is null and Close_t is not null and Time_UTC1<a.Time_UTC1 order by Time_UTC1 desc) b


select
	a.Time_UTC
	,b.Time_UTC
	,a.Open_t
	,b.[Open]
	,a.[Close_t]
	,b.[Close]
	,a.Volume_t
	,b.Volume
	,a.[High_t]
	,b.[High]
	,a.[Low_t]
	,b.[Low]
from 
#ticks_10sec a
full join #au_10sec_2014 b on a.time_utc = b.time_utc
where a.Open_t <> b.[Open]
	or a.Close_t <> b.[Close]
	or a.Volume_t <> b.Volume
	or a.High_t <> b.[High]
	or a.Low_t <> b.[Low]