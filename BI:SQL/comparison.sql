SET DATEFORMAT ymd
go
 create table #fund_2014
(
    Time_UTC    datetime,
    [Open]      numeric(8,3),
    [High]      numeric(8,3),
    [Low]       numeric(8,3),
    [Close]     numeric(8,3),
    Volume      numeric(8,5)
)
go
insert into #fund_2014
exec usp_get_new_candle_v2
	@tool = 'AU',
	@time_frame = 1,
    @unit = 'minute',
	@from_date = '2014-01-01 23:00:00.000',
	@to_date = '2014-01-04'

exec usp_get_new_candle 
	@tool = 'AU',
	@time_frame_in_minute = 1,
	@from_date = '2014-01-01 23:00:00.000',
	@to_date = '2014-01-04'

 select 
	proc_res.Time_UTC,
	true_Open = true_data.[Open],
	proc_res.[Open],
	true_High = true_data.[High],
	proc_res.[High],
	true_Low = true_data.[Low],
	proc_res.[Low],
	true_Close = true_data.[Close],
	proc_res.[Close],
	true_Volume = true_data.Volume,
	proc_res.[Volume]
 from [test].[fundAU2014] true_data
 full join #fund_2014 proc_res on proc_res.Time_UTC = true_data.Time_UTC
 where true_data.[Open] <> proc_res.[Open]
	   or true_data.[High] <> proc_res.[High]
	   or true_data.[Low] <> proc_res.[Low]
	   or true_data.[Close] <> proc_res.[Close]
	   or true_data.Volume <> proc_res.[Volume]
	   or true_data.[Open] is NULL
	   or proc_res.[Open] is null


-- select * from #fund_2014
 select * from [stg].[fund]
 where Time_UTC between '2014-01-01 23:00:00.000' and '2014-01-02'
 and name_tool = 'AU'