SET DATEFORMAT ymd
go
--��� ���������� ���� ��� �������� ��� � �������
create or alter procedure usp_get_new_candle 
	@tool varchar(8),
	@time_frame_in_minute int,
	@from_date datetime,
	@to_date datetime
as
begin
	with get_data_for_period 
		as
		(
				select 
					Name_tool,
					Time_UTC,
					[Open],
					[High],
					[Low], 
					[Close],
					Volume,
					Number = row_number() over(order by Time_UTC),
					cnt = count(*) over()
				from stg.fund 
				where Time_UTC between @from_date and @to_date
				and Name_tool = @tool
		)
	,set_of_numbers AS
		(
			 select num = 1,
					cnt
			 from get_data_for_period
			 where Number = 1
			 union all
			 select num + (@time_frame_in_minute)*60/10, 
					cnt
			 from set_of_numbers 
			 where num <= cnt
		)
	,data_with_numbers as
		(
			SELECT 
				Name_tool,
				Time_UTC,
				[Open],
				[High],
				[Low], 
				[Close],
				Volume,
				Number,
				num,
				max(case when num is not null then Number else num end) over (order by Number) as N
			from get_data_for_period a
			left join set_of_numbers b on a.Number = b.num
		)
	,get_calculated_data as
		(
		select 
			Name_tool,
			Time_UTC,
			[Open],
			maxHigh = max([High]) over(partition by N),
			minLow = min([Low]) over(partition by N),
			lastClose = lead([Close], (@time_frame_in_minute)*60/10-1) over(order by Time_UTC),
			sumVolume = sum(Volume) over(partition by N),
			num
		from data_with_numbers
		)
	select 
		Name_tool,
		Time_UTC,
		[Open],
		[High] = maxHigh,
		[Low] = minLow,
		[Close] = lastClose,
		Volume = sumVolume
	from get_calculated_data
	where num is not null
		  and lastClose is not null
	option(maxrecursion 0)
end
----------------------------------------------------------------------------------
go
exec usp_get_new_candle 
	@tool = 'AU',
	@time_frame_in_minute = 3,
	@from_date = '2012-01-01 23:00:00.000',
	@to_date = '2012-02-03 23:01:50.000'

