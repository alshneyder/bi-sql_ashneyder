create or alter procedure usp_get_new_candle_v2
	@tool varchar(8),
	@time_frame int,
	@unit varchar(10),
	@from_date datetime,
	@to_date datetime
as
begin
 with set_of_dates as 
	(
		select 
			convert(datetime, @from_date) as Time_UTC1
		union all
		select 
			case 
			when @unit = 'second' then dateadd(second, @time_frame, Time_UTC1)
			when @unit = 'minute' then dateadd(minute, @time_frame, Time_UTC1)
			when @unit = 'hour' then dateadd(hour, @time_frame, Time_UTC1)
			when @unit = 'week' then dateadd(week, @time_frame, Time_UTC1)
			when @unit = 'month' then dateadd(month, @time_frame, Time_UTC1)
			end
		from set_of_dates
		where case 
			when @unit = 'second' then dateadd(second, @time_frame, Time_UTC1)
			when @unit = 'minute' then dateadd(minute, @time_frame, Time_UTC1)
			when @unit = 'hour' then dateadd(hour, @time_frame, Time_UTC1)
			when @unit = 'week' then dateadd(week, @time_frame, Time_UTC1)
			when @unit = 'month' then dateadd(month, @time_frame, Time_UTC1)
			end <= @to_date
	) 
select * 
into #set_of_dates
from set_of_dates
option(maxrecursion 0)

delete from #set_of_dates
where (datename(WEEKDAY,Time_UTC1) in ('Monday', 'Tuesday', 'Wednesday', 'Thursday') and convert(time, Time_UTC1) between '22:00:00.00' and '22:59:59.00'
        and Time_UTC1 between '2014-01-01' and '2014-03-09 2:00:00' and Time_UTC1 between '2014-11-02 2:00:00' and '2014-12-31 23:59:59')
   or (datename(WEEKDAY,Time_UTC1) in ('Monday', 'Tuesday', 'Wednesday', 'Thursday') and convert(time, Time_UTC1) between '21:00:00.00' and '21:59:59.00'
        and Time_UTC1 between '2014-03-09 2:00:00' and '2014-11-02 2:00:00')
   or (datename(WEEKDAY,Time_UTC1) in ('Friday') and convert(time, Time_UTC1) between '22:00:00.00' and '23:59:59.00'
        and Time_UTC1 between '2014-01-01' and '2014-03-09 2:00:00' and Time_UTC1 between '2014-11-02 2:00:00' and '2014-12-31 23:59:59')
   or (datename(WEEKDAY,Time_UTC1) in ('Saturday'))
   or (datename(WEEKDAY,Time_UTC1) in ('Sunday') and convert(time, Time_UTC1) between '00:00:00.00' and '22:59:59.00'
        and Time_UTC1 between '2014-01-01' and '2014-03-09 2:00:00' and Time_UTC1 between '2014-11-02 2:00:00' and '2014-12-31 23:59:59')
   or (datename(WEEKDAY,Time_UTC1) in ('Friday') and convert(time, Time_UTC1) between '21:00:00.00' and '23:59:59.00'
        and Time_UTC1 between '2014-03-09 2:00:00' and '2014-11-02 2:00:00')
   or (datename(WEEKDAY,Time_UTC1) in ('Sunday') and convert(time, Time_UTC1) between '00:00:00.00' and '21:59:59.00'
        and Time_UTC1 between '2014-03-09 2:00:00' and '2014-11-02 2:00:00')
   or (Time_UTC1 between '2014-04-17 21:00:00' and '2014-04-20 21:59:59')
   or (Time_UTC1 between '2014-12-24 19:00:00' and '2014-12-25 22:59:59')

select
	Time_UTC
	,Bid
	,BidVolume
	,Part_of_Time = case 
	when @unit = 'second' then
		(case 
		when datepart(second, time_utc) >= 0 and datepart(second, time_utc) < 10 
		then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 0, 0)
		when datepart(second, time_utc) >= 10 and datepart(second, time_utc) < 20 
		then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 10, 0)
		when datepart(second, time_utc) >= 20 and datepart(second, time_utc) < 30 
		then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 20, 0)
		when datepart(second, time_utc) >= 30 and datepart(second, time_utc) < 40 
		then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 30, 0)
		when datepart(second, time_utc) >= 40 and datepart(second, time_utc) < 50 
		then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 40, 0)
		when datepart(second, time_utc) >= 50 and datepart(second, time_utc) <= 59 
		then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 50, 0)
		end)
	when @unit = 'minute' then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,time_utc), datepart(minute,time_utc), 0, 0)
	when @unit in ('hour', 'week', 'month') then datetimefromparts(year(time_utc), month(time_utc), day(time_utc), datepart(hour,Time_UTC), 0, 0, 0)
	end
into #ticks_for_period
from stg.fund_ticks
where Time_UTC between @from_date and @to_date
  and Name_tool = @tool

;with ticks_with_time_frame as 
    (
        select 
            Time_UTC = ISNULL(b.Time_UTC, a.Time_UTC1)
            ,b.Bid
            ,b.BidVolume
            ,Time_frame = ISNULL(b.Part_of_Time, a.Time_UTC1)
            ,a.Time_UTC1
        from #set_of_dates a
        left join #ticks_for_period b on b.Part_of_Time = a.[Time_UTC1]
    )
,firstV_and_lastV_for_ticks as 
	(
		select 
			Time_UTC
			,Bid
			,BidVolume
			,Time_frame
			,firstV = first_value(Bid) over(partition by Time_frame order by Time_UTC)
			,lastV = last_value(Bid) over(partition by Time_frame
								 	      order by Time_UTC  
								 	      rows between current row and unbounded following)
		from ticks_with_time_frame
	)
select 
	Time_UTC = Time_frame
	,Open_t = max(firstV)
	,Close_t = max(lastV)
	,Volume_t = sum(BidVolume)
	,High_t = max(Bid)
	,Low_t = min(Bid)
into #grouped_ticks_with_null
from firstV_and_lastV_for_ticks
group by Time_frame

select
	Time_UTC = isnull(Time_UTC, Time_UTC)
	,[Open] = isnull(Open_t, b.Close_t)
	,High = isnull(High_t, b.Close_t)
	,Low = isnull(Low_t, b.Close_t)
	,[Close] = isnull(a.Close_t, b.Close_t)
	,Volume = isnull(Volume_t, 0)
from #grouped_ticks_with_null a outer apply
(select top (1) Close_t
from #grouped_ticks_with_null
where a.Close_t is null and Close_t is not null and Time_UTC < a.Time_UTC order by Time_UTC desc) b
order by 1

drop table #set_of_dates
drop table #ticks_for_period
drop table #grouped_ticks_with_null
end
----------------------------------------------------------------------------------
go
exec usp_get_new_candle_v2
@tool = 'AU',
	@time_frame = 1,
    @unit = 'minute',
	@from_date = '2014-01-01 23:00:00.000',
	@to_date = '2014-01-04'

