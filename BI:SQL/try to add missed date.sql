SET DATEFORMAT ymd
go
with set_of_dates as 
	(
		select 
			1 as num, 
			convert(datetime, '2012-01-01 23:00:00.000') as Time_UTC1
		union all
		select 
			num + 1, 
			dateadd(second, 10, Time_UTC1)
		from set_of_dates
		where dateadd(second, 10, Time_UTC1) <= '2012-02-03 23:01:50.000'
	) --select * from set_of_dates
	--option(maxrecursion 0)

,fund as
	(
		select * 
		from stg.fund
		where Name_tool = 'AU'
			  and Time_UTC between '2012-01-01 23:00:00.000' and '2012-02-03 23:01:50.000'
	)
,get_all_data as
	(
		select 
			Time_UTC1, 
			Name_tool = isnull(Name_tool, 'AU'),
			Time_UTC = isnull(Time_UTC, Time_UTC1),
			[Open] = isnull([Open], 0),
			[High] = isnull([High], 0),
			[Low] = isnull([Low], 0), 
			[Close] = isnull([Close], 0),
			Volume = isnull(Volume, 0)
			--[weekday] = datename(weekday, Time_UTC1) 
		from set_of_dates a
		left join fund f on a.Time_UTC1 = f.Time_UTC
	)
select * from get_all_data
--where Time_UTC is null
order by Time_UTC1
option(maxrecursion 0)
