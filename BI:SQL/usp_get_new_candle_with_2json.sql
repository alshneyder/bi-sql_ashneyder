SET DATEFORMAT ymd
go
create or alter procedure usp_get_new_candle_with_2json
    @json_parameters nvarchar(max) = N'{
        "tool":"AU",
        "time_frame":1,
        "unit":"month",
        "from_date":"1990-01-01",
        "to_date":"2000-12-31"  
        }',
    @json_with_idle_info nvarchar(max) = N'{}'
as
begin

    with set_of_dates as 
        (
            select 
                convert(datetime, json_value(@json_parameters, '$.from_date')) as Time_UTC1
            union all
            select 
                case 
                when json_value(@json_parameters, '$.unit') = 'second' then dateadd(second, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                when json_value(@json_parameters, '$.unit') = 'minute' then dateadd(minute, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                when json_value(@json_parameters, '$.unit') = 'hour' then dateadd(hour, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                when json_value(@json_parameters, '$.unit') in ('day', 'week') then dateadd(day, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                when json_value(@json_parameters, '$.unit') = 'month' then dateadd(month, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                end
            from set_of_dates s
            where case 
                when json_value(@json_parameters, '$.unit') = 'second' then dateadd(second, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                when json_value(@json_parameters, '$.unit') = 'minute' then dateadd(minute, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                when json_value(@json_parameters, '$.unit') = 'hour' then dateadd(hour, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                when json_value(@json_parameters, '$.unit') in ('day', 'week') then dateadd(day, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                when json_value(@json_parameters, '$.unit') = 'month' then dateadd(month, convert(int, json_value(@json_parameters, '$.time_frame')), Time_UTC1)
                end <= json_value(@json_parameters, '$.to_date')
        )
    ,set_of_dates_with_week as (
        select Time_UTC1, 
        datepart(week, Time_UTC1) as weeknum 
        from set_of_dates
        )

    select Time_UTC1, weeknum 
    into #set_of_dates
    from set_of_dates_with_week
    option(maxrecursion 0)

    if json_value(@json_parameters, '$.unit') = 'week'
        begin
            select distinct Time_UTC1 = first_value(Time_UTC1) over(partition by weeknum order by Time_UTC1)
            into #set_of_dates_for_week
            from #set_of_dates
        end

    select 
        [day], 
        [start_date], 
        [end_date], 
        [idle_time_start], 
        [idle_time_end]
        into #weekday_info
    from openjson(@json_with_idle_info, '$.weekday')
        with(
            [day] nvarchar(10) '$.day',  
            [start_date] datetime '$.start_date',
            [end_date] datetime '$.end_date',
            [idle_time_start] time '$.idle_time_start',
            [idle_time_end] time '$.idle_time_end'
            )

    select
        [idle_datetime_start], 
        [idle_datetime_end] 
    into #special_day
    from openjson(@json_with_idle_info, '$.special_day')
        with(
            [idle_datetime_start] datetime '$.idle_datetime_start',  
            [idle_datetime_end] datetime '$.idle_datetime_end'
            )

    delete from #set_of_dates
    from #set_of_dates s
    join #weekday_info w on s.Time_UTC1 between w.start_date and w.end_date
        and convert(time, Time_UTC1) between w.idle_time_start and w.idle_time_end
        and datename(weekday,Time_UTC1) = w.day

    delete from #set_of_dates
    from #set_of_dates s
    join #special_day sd on s.Time_UTC1 between sd.[idle_datetime_start] and sd.[idle_datetime_end]

    ;with min_max_Time_UTC as (
        select 
            min_Time_UTC = min(Time_UTC),
            max_Time_UTC = max(Time_UTC)
        from stg.fund_ticks
        where Name_tool = json_value(@json_parameters, '$.tool')
    )
    delete from #set_of_dates
    where Time_UTC1 < (
        select min_Time_UTC
        from min_max_Time_UTC
        )
    or Time_UTC1 > (
        select max_Time_UTC
        from min_max_Time_UTC
        )

    ;with ticks_for_period as (
        select
            Time_UTC
            ,Bid
            ,BidVolume
            ,Weeknum = datepart(week, Time_UTC)
            ,Part_of_Time = case 
            when json_value(@json_parameters, '$.unit') = 'second' then
                (case 
                when datepart(second, Time_UTC) >= 0 and datepart(second, Time_UTC) < 10 
                then datetimefromparts(year(Time_UTC), month(Time_UTC), day(Time_UTC), datepart(hour,Time_UTC), datepart(minute,Time_UTC), 0, 0)
                when datepart(second, Time_UTC) >= 10 and datepart(second, Time_UTC) < 20 
                then datetimefromparts(year(Time_UTC), month(Time_UTC), day(Time_UTC), datepart(hour,Time_UTC), datepart(minute,Time_UTC), 10, 0)
                when datepart(second, Time_UTC) >= 20 and datepart(second, Time_UTC) < 30 
                then datetimefromparts(year(Time_UTC), month(Time_UTC), day(Time_UTC), datepart(hour,Time_UTC), datepart(minute,Time_UTC), 20, 0)
                when datepart(second, Time_UTC) >= 30 and datepart(second, Time_UTC) < 40 
                then datetimefromparts(year(Time_UTC), month(Time_UTC), day(Time_UTC), datepart(hour,Time_UTC), datepart(minute,Time_UTC), 30, 0)
                when datepart(second, Time_UTC) >= 40 and datepart(second, Time_UTC) < 50 
                then datetimefromparts(year(Time_UTC), month(Time_UTC), day(Time_UTC), datepart(hour,Time_UTC), datepart(minute,Time_UTC), 40, 0)
                when datepart(second, Time_UTC) >= 50 and datepart(second, Time_UTC) <= 59 
                then datetimefromparts(year(Time_UTC), month(Time_UTC), day(Time_UTC), datepart(hour,Time_UTC), datepart(minute,Time_UTC), 50, 0)
                end)
            when json_value(@json_parameters, '$.unit') = 'minute' then datetimefromparts(year(Time_UTC), month(Time_UTC), day(Time_UTC), datepart(hour,Time_UTC), datepart(minute,Time_UTC), 0, 0)
            when json_value(@json_parameters, '$.unit') = 'hour' then datetimefromparts(year(Time_UTC), month(Time_UTC), day(Time_UTC), datepart(hour,Time_UTC), 0, 0, 0)
            when json_value(@json_parameters, '$.unit') in ('day', 'week') then datetimefromparts(year(Time_UTC), month(Time_UTC), day(Time_UTC), 0, 0, 0, 0)
            when json_value(@json_parameters, '$.unit') = 'month' then datetimefromparts(year(Time_UTC), month(Time_UTC), 1, 0, 0, 0, 0)
            end
        from stg.fund_ticks
        where Time_UTC between json_value(@json_parameters, '$.from_date') and json_value(@json_parameters, '$.to_date')
        and Name_tool = json_value(@json_parameters, '$.tool')
        ) 
    select 
        Time_UTC
        ,Bid
        ,BidVolume
        ,Part_of_Time
        ,Part_of_Time_for_week = first_value(Part_of_Time) over(partition by Weeknum order by Time_UTC)
    into #ticks_for_period
    from ticks_for_period

    if json_value(@json_parameters, '$.unit') = 'week'
        begin
            with ticks_with_time_frame_for_week as (
                select 
                    Time_UTC = ISNULL(b.Time_UTC, a.Time_UTC1)
                    ,b.Bid
                    ,b.BidVolume
                    ,Time_frame = ISNULL(b.Part_of_Time_for_week, a.Time_UTC1)
                    ,a.Time_UTC1  
                from #set_of_dates_for_week a
                left join #ticks_for_period b on b.Part_of_Time_for_week = a.[Time_UTC1]
            )
            ,firstV_and_lastV_for_ticks_for_week as 
                (
                    select 
                        Time_UTC
                        ,Bid
                        ,BidVolume
                        ,Time_frame
                        ,firstV = first_value(Bid) over(partition by Time_frame order by Time_UTC)
                        ,lastV = last_value(Bid) over(partition by Time_frame
                                                    order by Time_UTC  
                                                    rows between current row and unbounded following)
                    from ticks_with_time_frame_for_week
                )
            select 
                Time_UTC = Time_frame
                ,Open_t = max(firstV)
                ,Close_t = max(lastV)
                ,Volume_t = sum(BidVolume)
                ,High_t = max(Bid)
                ,Low_t = min(Bid)
            into #grouped_ticks_with_null_for_week
            from firstV_and_lastV_for_ticks_for_week
            group by Time_frame

            select
                Name_tool = json_value(@json_parameters, '$.tool')
                ,Time_UTC
                ,[Open] = isnull(Open_t, b.Close_t)
                ,High = isnull(High_t, b.Close_t)
                ,Low = isnull(Low_t, b.Close_t)
                ,[Close] = isnull(a.Close_t, b.Close_t)
                ,Volume = isnull(Volume_t, 0)
            from #grouped_ticks_with_null_for_week a outer apply
            (select top (1) Close_t
            from #grouped_ticks_with_null_for_week
            where a.Close_t is null 
                and Close_t is not null 
                and Time_UTC < a.Time_UTC 
            order by Time_UTC desc) b
            order by Time_UTC

            drop table #set_of_dates_for_week
            drop table #grouped_ticks_with_null_for_week
        end
    else 
        begin
            with ticks_with_time_frame as (
                select 
                    Time_UTC = ISNULL(b.Time_UTC, a.Time_UTC1)
                    ,b.Bid
                    ,b.BidVolume
                    ,Time_frame = ISNULL(b.Part_of_Time, a.Time_UTC1)
                    ,a.Time_UTC1  
                from #set_of_dates a
                left join #ticks_for_period b on b.Part_of_Time = a.[Time_UTC1]
            )
            ,firstV_and_lastV_for_ticks as 
                (
                    select 
                        Time_UTC
                        ,Bid
                        ,BidVolume
                        ,Time_frame
                        ,firstV = first_value(Bid) over(partition by Time_frame order by Time_UTC)
                        ,lastV = last_value(Bid) over(partition by Time_frame
                                                    order by Time_UTC  
                                                    rows between current row and unbounded following)
                    from ticks_with_time_frame
                )
            select 
                Time_UTC = Time_frame
                ,Open_t = max(firstV)
                ,Close_t = max(lastV)
                ,Volume_t = sum(BidVolume)
                ,High_t = max(Bid)
                ,Low_t = min(Bid)
            into #grouped_ticks_with_null
            from firstV_and_lastV_for_ticks
            group by Time_frame

            select
                Name_tool = json_value(@json_parameters, '$.tool')
                ,Time_UTC = isnull(Time_UTC, Time_UTC)
                ,[Open] = isnull(Open_t, b.Close_t)
                ,High = isnull(High_t, b.Close_t)
                ,Low = isnull(Low_t, b.Close_t)
                ,[Close] = isnull(a.Close_t, b.Close_t)
                ,Volume = isnull(Volume_t, 0)
            from #grouped_ticks_with_null a outer apply
            (select top (1) Close_t
            from #grouped_ticks_with_null
            where a.Close_t is null 
                and Close_t is not null 
                and Time_UTC < a.Time_UTC 
            order by Time_UTC desc) b
            order by Time_UTC

            drop table #grouped_ticks_with_null
        end

    drop table #set_of_dates
    drop table #weekday_info
    drop table #special_day
    drop table #ticks_for_period
end
----------------------------------------------------------------------------------
go
exec usp_get_new_candle_with_2json
@json_parameters = N'{
        "tool":"AU",
        "time_frame":1,
        "unit":"hour",
        "from_date":"2014-01-01",
        "to_date":"2014-01-31"  
        }',

    @json_with_idle_info =
N'{
"weekday":[
    {
        "day":"Monday", 
        "start_date":"2014-01-01 00:00:00",
        "end_date":"2014-03-09 2:00:00",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"22:59:59.00"
    },
    {
        "day":"Monday", 
        "start_date":"2014-03-09 2:00:00",
        "end_date":"2014-11-02 2:00:00",
        "idle_time_start":"21:00:00.00",
        "idle_time_end":"21:59:59.00"
    },
    {
        "day":"Monday", 
        "start_date":"2014-11-02 2:00:00",
        "end_date":"2014-12-31 23:59:59",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"22:59:59.00"
    },
    {
        "day":"Tuesday", 
        "start_date":"2014-01-01 00:00:00",
        "end_date":"2014-03-09 2:00:00",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"22:59:59.00"
    },
    {
        "day":"Tuesday", 
        "start_date":"2014-03-09 2:00:00",
        "end_date":"2014-11-02 2:00:00",
        "idle_time_start":"21:00:00.00",
        "idle_time_end":"21:59:59.00"
    },
    {
        "day":"Tuesday", 
        "start_date":"2014-11-02 2:00:00",
        "end_date":"2014-12-31 23:59:59",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"22:59:59.00"
    },
    {
        "day":"Wednesday", 
        "start_date":"2014-01-01 00:00:00",
        "end_date":"2014-03-09 2:00:00",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"22:59:59.00"
    },
    {
        "day":"Wednesday", 
        "start_date":"2014-03-09 2:00:00",
        "end_date":"2014-11-02 2:00:00",
        "idle_time_start":"21:00:00.00",
        "idle_time_end":"21:59:59.00"
    },
    {
        "day":"Wednesday", 
        "start_date":"2014-11-02 2:00:00",
        "end_date":"2014-12-31 23:59:59",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"22:59:59.00"
    },
    {
        "day":"Thursday", 
        "start_date":"2014-01-01 00:00:00",
        "end_date":"2014-03-09 2:00:00",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"22:59:59.00"
    },
    {
        "day":"Thursday",
        "start_date":"2014-03-09 2:00:00",
        "end_date":"2014-11-02 2:00:00",
        "idle_time_start":"21:00:00.00",
        "idle_time_end":"21:59:59.00"
    },
    {
        "day":"Thursday", 
        "start_date":"2014-11-02 2:00:00",
        "end_date":"2014-12-31 23:59:59",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"22:59:59.00"
    },
    {
        "day":"Friday",
        "start_date":"2014-01-01 00:00:00",
        "end_date":"2014-03-09 2:00:00",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"23:59:59.00"
    },
    {
        "day":"Friday",
        "start_date":"2014-03-09 2:00:00",
        "end_date":"2014-11-02 2:00:00",
        "idle_time_start":"21:00:00.00",
        "idle_time_end":"23:59:59.00"
    },
    {
        "day":"Friday",
        "start_date":"2014-11-02 2:00:00",
        "end_date":"2014-12-31 23:59:59",
        "idle_time_start":"22:00:00.00",
        "idle_time_end":"23:59:59.00"
    },
    {
        "day":"Saturday",
        "start_date":"2014-01-01 00:00:00",
        "end_date":"2014-03-09 2:00:00",
        "idle_time_start":"00:00:00.00",
        "idle_time_end":"23:59:59.00"
    },
    {
        "day":"Saturday",
        "start_date":"2014-03-09 2:00:00",
        "end_date":"2014-11-02 2:00:00",
        "idle_time_start":"00:00:00.00",
        "idle_time_end":"23:59:59.00"
    },
    {
        "day":"Saturday",
        "start_date":"2014-11-02 2:00:00",
        "end_date":"2014-12-31 23:59:59",
        "idle_time_start":"00:00:00.00",
        "idle_time_end":"23:59:59.00"
    },
    {
        "day":"Sunday",
        "start_date":"2014-01-01 00:00:00",
        "end_date":"2014-03-09 2:00:00",
        "idle_time_start":"00:00:00.00",
        "idle_time_end":"22:59:59.00"
    },
    {
        "day":"Sunday",
        "start_date":"2014-03-09 2:00:00",
        "end_date":"2014-11-02 2:00:00",
        "idle_time_start":"00:00:00.00",
        "idle_time_end":"21:59:59.00"
    },
    {
        "day":"Sunday",
        "start_date":"2014-11-02 2:00:00",
        "end_date":"2014-12-31 23:59:59",
        "idle_time_start":"00:00:00.00",
        "idle_time_end":"22:59:59.00"
    }
    ],
    "special_day":[
    {
        "idle_datetime_start":"2014-04-17 21:00:00",
            "idle_datetime_end":"2014-04-20 21:59:59"
        },
        {
            "idle_datetime_start":"2014-12-24 19:00:00",
            "idle_datetime_end":"2014-12-25 22:59:59"
        }
        ]
}'



