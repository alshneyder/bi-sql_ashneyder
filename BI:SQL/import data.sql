CREATE DATABASE fund
go
use fund
go
create schema stg
go
create table stg.fund
(
    ID          int identity primary key,
    Name_tool   nvarchar(8), 
    Time_UTC    datetime,
    [Open]      numeric(8,3),
    [High]      numeric(8,3),
    [Low]       numeric(8,3),
    [Close]     numeric(8,3),
    Volume      numeric(8,5)
)
GO

create table #lnd_AU_fund
(
    Time_UTC    datetime,
    [Open]      varchar(8),
    [High]      varchar(8),
    [Low]       varchar(8),
    [Close]     varchar(8),
    Volume      varchar(8)
)

GO
create table #lnd_USA500_fund
(
    Time_UTC    datetime,
    [Open]      varchar(8),
    [High]      varchar(8),
    [Low]       varchar(8),
    [Close]     varchar(8),
    Volume      varchar(8)
)
GO
--AU-----------------------------------------------------
declare @files table ([filename] varchar(255), depth smallint, isfile smallint)
declare @sql varchar(max)
declare @filename varchar(255) 
declare @path varchar(255) = 'D:\BI\IsSoft\fund\AU'

insert into @files ([filename],depth,isfile)
exec xp_dirtree @path, 1, 1

while exists (select top 1 1 from @files)
begin
	select top 1 @filename = filename from @files 

	set @sql = 'bulk insert #lnd_au_fund from ''' + @path +'\'+ @filename + ''' with (fieldterminator = '';'', rowterminator = ''\n'', firstrow = 2 ) '
	print @sql
	exec (@sql)

	delete @files where [filename] = @filename 
	set @filename = ''
end

declare @files1 table ([filename] varchar(255), depth smallint, isfile smallint)
declare @sql1 varchar(max)
declare @filename1 varchar(255) 
declare @path1 varchar(255) = 'D:\BI\IsSoft\fund\USA500'

--USA500-------------------------------------------------------------------------
insert into @files1 ([filename],depth,isfile)
exec xp_dirtree @path1, 1, 1

while exists (select top 1 1 from @files1)
begin
	select top 1 @filename1 = filename from @files1

	set @sql1 = 'bulk insert #lnd_usa500_fund from ''' + @path1 +'\'+ @filename1 + ''' with (fieldterminator = '';'', rowterminator = ''\n'', firstrow = 2 ) '
	print @sql1
	exec (@sql1)

	delete @files1 where [filename] = @filename1 
	set @filename1 = ''
end

insert into stg.fund
select 'AU' as Name_tool,
        Time_UTC,
        convert(NUMERIC(8, 3), replace([Open],',','.')) as [Open],
        convert(NUMERIC(8, 3), replace([High],',','.')) as [High],
        convert(NUMERIC(8, 3), replace([Low],',','.')) as [Low],
        convert(NUMERIC(8, 3), replace([Close],',','.')) as [Close],
        convert(NUMERIC(8, 5), replace([Volume],',','.')) as [Volume]
from #lnd_AU_fund
union all
select 'USA500' as Name_tool,
        Time_UTC,
        convert(NUMERIC(8, 3), replace([Open],',','.')) as [Open],
        convert(NUMERIC(8, 3), replace([High],',','.')) as [High],
        convert(NUMERIC(8, 3), replace([Low],',','.')) as [Low],
        convert(NUMERIC(8, 3), replace([Close],',','.')) as [Close],
        convert(NUMERIC(8, 5), replace([Volume],',','.')) as [Volume]
from #lnd_USA500_fund
order by Name_tool, Time_UTC



