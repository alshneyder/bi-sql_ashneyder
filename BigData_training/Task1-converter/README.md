## Description

The console utility supports converting csv files to parquet files and vice versa, and also can show schema of csv or parquet file (list of attributes and data types).

## Libraries
Before running the utility, install the following libraries
```bash
///use pip or pip3 (example for pip3)
pip3 install sys
pip3 install argparse
pip3 install pandas
pip3 install pyarrow
```
## Usage
```bash
python3 converter.py [-h] [--csv2parquet <src-filename> <dst-filename>] [--parquet2csv <src-filename> <dst-filename>] [--schema <filename>]
```

## Options
```bash
options:
  -h, --help            show this help message and exit
  --csv2parquet <src-filename> <dst-filename>
                        convert csv file to parquet file
  --parquet2csv <src-filename> <dst-filename>
                        convert parquet file to csv file
  --schema <filename>   show file schema
```