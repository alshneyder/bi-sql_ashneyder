import sys
import argparse
import pandas as pd
import pyarrow


def main():
    converter = argparse.ArgumentParser(
        prog='converter.py',
        description='''Supports converting csv files to parquet files and vice vers,
        and also can show schema of csv or parquet file (list of attributes and data types).'''
    )
    converter.add_argument('--csv2parquet',
                           nargs=2,
                           metavar=('<src-filename>', '<dst-filename>'),
                           help='convert csv file to parquet file')
    converter.add_argument('--parquet2csv',
                           nargs=2,
                           metavar=('<src-filename>', '<dst-filename>'),
                           help='convert parquet file to csv file')
    converter.add_argument('--schema',
                           nargs=1,
                           metavar='<filename>',
                           help='show file schema')

    return converter


def func_csv2parquet(src_filename, dst_filename):
    df = pd.read_csv(src_filename)
    df.to_parquet(dst_filename)


def func_parquet2csv(src_filename, dst_filename):
    df = pd.read_parquet(src_filename)
    df.to_csv(dst_filename, index=False)


def func_schema(filename):
    if filename.endswith('csv'):
        df = pd.read_csv(filename)
        print(df.dtypes)
    elif filename.endswith('parquet'):
        df = pd.read_parquet(filename)
        print(df.dtypes)


if __name__ == '__main__':
    converter = main()
    argument = converter.parse_args(sys.argv[1:])

    if argument.csv2parquet:
        func_csv2parquet(*argument.csv2parquet)
    elif argument.parquet2csv:
        func_parquet2csv(*argument.parquet2csv)
    elif argument.schema:
        func_schema(*argument.schema)



